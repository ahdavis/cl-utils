;;;; macros.lisp
;;;; Defines macros for cl-utils
;;;; Created by Andrew Davis
;;;; Created on 8/28/2019
;;;; Copyright (C) 2019  Andrew Davis
;;;;
;;;; This program is free software: you can redistribute it and/or modify   
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; enter the cl-utils package
(in-package cl-utils)

;;; This macro binds variables to gensyms
(defmacro with-gensyms (vars &body body)
  "Binds 'vars' to generic symbols in 'body'"
  `(let ,(loop for v in vars collect `(,v (gensym)))
     ,@body))

;;; end of file
