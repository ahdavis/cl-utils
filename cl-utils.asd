;;;; cl-utils.asd
;;;; ASDF config file for cl-utils
;;;; Created by Andrew Davis
;;;; Created on 8/28/2019
;;;; Copyright (C) 2019  Andrew Davis
;;;;
;;;; This program is free software: you can redistribute it and/or modify   
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; define the system
(asdf:defsystem :cl-utils
 :description "Utilities for Common Lisp"
 :version "1.0"
 :author "Andrew Davis"
 :license "LGPL-3.0+"
 :serial t
 :components ((:module "src"
		:components ((:file "packages")
				(:file "macros" :depends-on ("packages"))))))

;;; end of file
